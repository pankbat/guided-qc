import React, { PureComponent } from 'react';
import RedoQC from './RedoQC';
import Summary from './Summary';

class SummaryLayout extends PureComponent {
  constructor(props) {
    super(props);
    this.onKeydown = this.onKeydown.bind(this);
  }
  componentDidMount() {
    document.addEventListener('keydown', this.onKeydown);
  }
  componentWillUnmount() {
    document.removeEventListener('keydown', this.onKeydown);
  }
  onKeydown(event) {
    if (event.which === 89) {
      this.props.submit();
    }
    event.stopPropagation();
  }
  render() {
    return (
      <div className="guided-qc">
        <div className="x-heading" style={{ padding: '20px 0', fontSize: '25px', textAlign: 'left' }}>Marking as QC Fail</div>
        <Summary list={this.props.userEntries} />
        <button className="x-btn x-btn-primary display-block mr-auto" style={{ marginTop: '50px', marginBottom: '20px' }} onClick={this.props.submit}>
          <div className="fw500 fsp20">Confirm</div>
          <div className="fw300 fsp14 mr-t5 italic">Press&nbsp;<b>Y</b>&nbsp;to confirm</div>
        </button>
        <RedoQC onRedo={this.props.onRedo} />
      </div>
    );
  }
}

export default SummaryLayout;
