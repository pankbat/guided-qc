import './loader.css';
import React from 'react';

const Loader = () => {
  return (
    <div className="u-loader-loader u-loader-large">
      <div className="u-loader-spinner u-loader-is-loading">
        <div></div>
        <div></div>
        <div></div>
      </div>
    </div>
  );
};

export default Loader;
