import React, { PureComponent } from 'react';
import ProductImageQC from './ProductImageQC';

class ImageMismatch extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
    this.onKeydown = this.onKeydown.bind(this);
  }
  componentDidMount() {
    document.addEventListener('keydown', this.onKeydown);
  }
  componentWillUnmount() {
    document.removeEventListener('keydown', this.onKeydown);
  }
  onKeydown(event) {
    if (event.which === 89) { // Y/y (89) to Yes
      this.setValue('YES');
    } else if (event.which === 78) { // n/N (78) to No
      this.setValue('NO');
    }
    event.stopPropagation();
  }
  onClick(value) {
    this.setValue(value);
  }
  setValue(value) {
    this.setState({ active: value }, () => {
      setTimeout(() => {
        this.props.setValue(value);
        this.setState({ active: '' });
      }, 500);
    });
  }
  render() {
    const styles = this.props.styles || [];
    const { active } = this.state;
    return (
      <div>
        <div style={{ display: 'flex', justifyContent: 'center' }}>
          <ProductImageQC styles={styles} />
        </div>
        <div>
          <button className={`x-btn x-btn-default ${active === 'YES' ? 'x-active' : ''}`} style={{ marginRight: '10px', minWidth: '200px' }} onClick={() => this.onClick('YES')}>
            <div className="fw500 fsp20">Yes</div>
            <div className="fw300 fsp14 mr-t5 italic">Press&nbsp;<b>Y</b>&nbsp;for Yes</div>
          </button>
          <button className={`x-btn x-btn-default ${active === 'NO' ? 'x-active' : ''}`} style={{ minWidth: '200px' }} onClick={() => this.onClick('NO')}>
            <div className="fw500 fsp20">No</div>
            <div className="fw300 fsp14 mr-t5 italic">Press&nbsp;<b>N</b>&nbsp;for No</div>
          </button>
        </div>
      </div>
    );
  }
}

export default ImageMismatch;
