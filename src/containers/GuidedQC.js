import '../styles/app.css';
import React, { PureComponent } from 'react';
import RuleEngine from '../utils/rule-engine';
import { get } from '../utils/get';
// components
import BooleanComponent from '../components/Boolean';
import TextInput from '../components/TextInput';
import Random from '../components/Random';
import Barcode from '../components/Barcode';
import MRP from '../components/MRP';
import Sizing from '../components/Sizing';
import SummaryLayout from '../components/SummaryLayout';
import RedoQC from '../components/RedoQC';
import Loader from '../components/Loader';
import ImageMismatch from '../components/ImageMismatch';

const COMPONENTS = {
  BARCODE: Barcode,
  RANDOM: Random,
  TEXT: TextInput,
  BOOLEAN: BooleanComponent,
  MRP: MRP,
  SIZING: Sizing,
  IMAGE_MISMATCH: ImageMismatch
};

class GuidedQC extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { current: 0, doShowSummary: false, viewTime: (new Date()).toISOString() };
    this.userEntries = [];
    this.onCaptureInput = this.onCaptureInput.bind(this);
    this.onRedo = this.onRedo.bind(this);
    this.submit = this.submit.bind(this);
  }
  componentDidMount() {
    this.ruleEngine = new RuleEngine();
  }
  componentDidUpdate(prevProps) {
    const { actionCodes } = this.props;
    if (prevProps.actionCodes !== actionCodes && actionCodes.length) {
      const doShowSummary = actionCodes.some(item => {
        let qcStatus = get(item, 'actionCodeMetaData.properties.qcStatus');
        return qcStatus === 'QC_FAIL';
      })
      this.setState({ doShowSummary, validating: false });
      if (!doShowSummary) {
        this.submit();
      }
    }
  }
  getEntry({ input, userActionEntry, entry, viewTime }) {
    return {
      input,
      uiDerivedActionCode: userActionEntry.actionCode,
      viewTime,
      inputTime: (new Date()).toISOString(),
      enrichedCheckEntry: entry
    };
  }
  onCaptureInput(input) {
    if (!input) return;
    const { current, viewTime } = this.state;
    const entry = get(this.props.metaData, `enrichedCheckEntries.${current}`, {});
    const actionEntries = get(entry, `checkActionsEntry.actionEntries`, []);
    const totalChecks = get(this.props.metaData, 'enrichedCheckEntries.length');

    const userActionEntry = this.ruleEngine.apply(input, actionEntries);
    if (current < totalChecks) {
      this.userEntries.push(this.getEntry({ input, userActionEntry, entry, viewTime }));
    }

    const failType = get(this.props.metaData, 'failType');
    if (failType === 'FIRST' && userActionEntry.qcStatus === 'QC_FAIL') {
      this.validateUserEntries(this.userEntries);
      return;
    }

    if (current + 1 === totalChecks) { // on complete all checks
      this.validateUserEntries(this.userEntries);
    }

    setTimeout(() => this.next(), 100);
  }
  next() {
    let current = this.state.current + 1;
    this.setState({ current, viewTime: (new Date()).toISOString() });
  }
  validateUserEntries() {
    this.setState({ validating: true });
    this.props.validateUserEntries(this.userEntries);
  }
  onRedo() {
    const { onRedo } = this.props;
    this.userEntries = [];
    this.setState({ current: 0, doShowSummary: false });
    if (typeof onRedo === 'function') onRedo();
  }
  submit() {
    const { onSubmit } = this.props;
    if (typeof onSubmit === 'function') {
      onSubmit(this.userEntries);
    }
  }
  render() {
    const { current, doShowSummary, validating } = this.state;
    const entry = get(this.props.metaData, `enrichedCheckEntries.${current}.checkDetailsEntry`, {});
    const values = get(this.props.metaData, `enrichedCheckEntries.${current}.values`, []);
    const totalChecks = get(this.props.metaData, 'enrichedCheckEntries.length');
    const styles = get(this.props, 'styles', []);
    const { defaultDisplayText: question, checkType: type } = entry;
    const guideline = get(entry, 'guidelineEntries.0.resource')

    if (validating) return <div className="guided-qc"><Loader /></div>;

    if (doShowSummary) {
      return <SummaryLayout userEntries={this.userEntries} onRedo={this.onRedo} submit={this.submit} />;
    }

    return (
      <div className="guided-qc flex-column" style={{ paddingTop: '45px', position: 'relative' }}>
        <div style={{ position: 'absolute', right: '30px', top: '0', fontWeight: 'bold' }}>{current + 1}/{totalChecks}</div>
        <div className="x-heading display-block mr-b50">{question}</div>
        <div className={`x-input-container-${current}`}>
          {COMPONENTS[type] && (
            React.createElement(COMPONENTS[type], {
              setValue: this.onCaptureInput,
              values: values,
              styles,
              guideline,
              itemBarcode: this.props.itemBarcode,
              scannerTimeout: this.props.scannerTimeout
            })
          )}
        </div>
        <RedoQC onRedo={this.onRedo} />
      </div>
    );
  }
}

export default GuidedQC;
