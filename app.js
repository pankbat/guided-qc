import React from 'react';
import ReactDOM from 'react-dom';
import GuidedQC from './src/containers/GuidedQC';
import { checksToPerform } from './static/getChecksToPerform';

export default ReactDOM.render(<GuidedQC metaData={checksToPerform.data[0]} />, document.getElementById('app'));
