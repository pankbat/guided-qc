import React, { PureComponent } from 'react';
// components
import ImageGallery from './ImageGallery'
import Modal from './Modal';

class ProductImageQC extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { isZoomOpen: false };
    this.open = this.openCloseZoomModal.bind(this, true);
    this.onClose = this.openCloseZoomModal.bind(this, false);
  }
  openCloseZoomModal(isZoomOpen) {
    this.setState({ isZoomOpen });
  }
  render() {
    const { isZoomOpen } = this.state;
    const { styles } = this.props;
    return (
      <div style={{ paddingBottom: '20px' }}>
        {styles && styles.length ? <ImageGallery list={styles} height="52vh" imageToShow="image" onClickImage={this.open} /> : <div style={{ height: '52vh' }} className="no-preview"><img src="https://myntrascmuistatic.myntassets.com//babylon-assets/images/qc-no-preview-available.png" /></div>}
        {styles  && styles.length ? <Modal isOpen={isZoomOpen} onClose={this.onClose} className="guidedqc-modals zoom-modal">
          <ImageGallery list={styles} height="70vh" imageToShow="zoom" />
        </Modal> : null}
      </div>
    );
  }
}

export default ProductImageQC;