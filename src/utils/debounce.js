export default function debounce(callback, delay) {
  let timeout;
  return function() {
    clearTimeout(timeout);
    const args = Array.prototype.slice.call(arguments);
    timeout = setTimeout(() => callback.apply(this, args), delay);
  }
}