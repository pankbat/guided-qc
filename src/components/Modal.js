import React, { PureComponent } from 'react';

class Modal extends PureComponent {
  constructor(props) {
    super(props);
  }
  render() {
    const { isOpen, onClose, children, className = '' } = this.props;

    if (!isOpen) return null;

    return (
      <div className={`u-modal-modal ${className}`}>
        <div className="u-modal-backdrop"></div>
        <div className="u-modal-body">
          <div className="u-modal-content">
            <div className="u-modal-wrapper">
              {children}
            </div>
          </div>
          <div className="u-modal-close">
            <button onClick={onClose} tabIndex="0" title="close" type="button" className="u-button-container u-button-text regular u-button-inherit u-button-icon" role="button" data-test-id="target" style={{ textTransform: 'uppercase' }}>
              <span className="u-button-icon" data-test-id="primary-icon"><svg aria-hidden="true" className="u-icon-svg" xmlns="http://www.w3.org/2000/svg">
                <use xlinkHref="#uikit-i-times"></use></svg>
              </span>
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default Modal;
