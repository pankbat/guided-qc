import React from 'react';

const RedoQC = props => {
  return (
    <div className="text-center" style={{ marginTop: 'auto', padding: '20px' }}>
      <span className="inline-block mr-r5">I want to redo the QC</span>
      <a className="inline-block x-btn-link" onClick={props.onRedo}>Start Again</a>
    </div>
  );
};

export default RedoQC;
