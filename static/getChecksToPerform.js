export const checksToPerform = {
  "status": {
      "statusCode": 118,
      "statusMessage": "All checks to perform retrieved successfully",
      "statusType": "SUCCESS",
      "totalCount": 12
  },
  "data": [
      {
          "failType": "LAST",
          "enrichedCheckEntries": [
              {
                  "checkId": 120,
                  "ruleIds": [
                      "c36b0dc0-646c-11ea-be51-a18b1ffbcaa3",
                      "90d6e730-646c-11ea-be51-a18b1ffbcaa3"
                  ],
                  "checkDetailsEntry": {
                      "defaultDisplayText": "Scan the WMS sticker and GTIN/SKU on the product",
                      "checkType": "BARCODE"
                  },
                  "checkActionsEntry": {
                      "actionEntries": [
                          {
                              "expression": {
                                  "value": "SKU353765",
                                  "operator": "EQUALS"
                              },
                              "actionCodeMetaData": {
                                  "actionCode": "CORRECT_PRODUCT",
                                  "properties": {
                                      "qcStatus": "QC_PASS"
                                  }
                              }
                          },
                          {
                              "expression": {
                                  "value": "SKU353765",
                                  "operator": "NOT_EQUALS"
                              },
                              "actionCodeMetaData": {
                                  "actionCode": "STICKER_MISMATCH",
                                  "properties": {
                                      "qcStatus": "QC_FAIL"
                                  }
                              }
                          }
                      ]
                  }
              },
              {
                  "checkId": 116,
                  "ruleIds": [
                      "c36b0dc0-646c-11ea-be51-a18b1ffbcaa3",
                      "90d6e730-646c-11ea-be51-a18b1ffbcaa3"
                  ],
                  "values": [
                      "yes",
                      "no"
                  ],
                  "checkDetailsEntry": {
                      "defaultDisplayText": "Check if Image Available on screen",
                      "checkType": "BOOLEAN"
                  },
                  "checkActionsEntry": {
                      "actionEntries": [
                          {
                              "expression": {
                                  "value": "yes",
                                  "operator": "EQUALS"
                              },
                              "actionCodeMetaData": {
                                  "actionCode": "CORRECT_PRODUCT",
                                  "properties": {
                                      "qcStatus": "QC_PASS"
                                  }
                              }
                          },
                          {
                              "expression": {
                                  "value": "yes",
                                  "operator": "NOT_EQUALS"
                              },
                              "actionCodeMetaData": {
                                  "actionCode": "CATALOG_MISSING",
                                  "properties": {
                                      "qcStatus": "QC_FAIL"
                                  }
                              }
                          }
                      ]
                  }
              },
              {
                  "checkId": 117,
                  "ruleIds": [
                      "c36b0dc0-646c-11ea-be51-a18b1ffbcaa3",
                      "90d6e730-646c-11ea-be51-a18b1ffbcaa3"
                  ],
                  "values": [
                      "yes",
                      "no"
                  ],
                  "checkDetailsEntry": {
                      "defaultDisplayText": "Check if packaging box is sellable",
                      "checkType": "BOOLEAN"
                  },
                  "checkActionsEntry": {
                      "actionEntries": [
                          {
                              "expression": {
                                  "value": "yes",
                                  "operator": "EQUALS"
                              },
                              "actionCodeMetaData": {
                                  "actionCode": "CORRECT_PRODUCT",
                                  "properties": {
                                      "qcStatus": "QC_PASS"
                                  }
                              }
                          },
                          {
                              "expression": {
                                  "value": "yes",
                                  "operator": "NOT_EQUALS"
                              },
                              "actionCodeMetaData": {
                                  "actionCode": "BOX_DAMAGE",
                                  "properties": {
                                      "qcStatus": "QC_FAIL"
                                  }
                              }
                          }
                      ]
                  }
              },
              {
                  "checkId": 118,
                  "ruleIds": [
                      "c36b0dc0-646c-11ea-be51-a18b1ffbcaa3",
                      "90d6e730-646c-11ea-be51-a18b1ffbcaa3"
                  ],
                  "values": [
                      "yes",
                      "no"
                  ],
                  "checkDetailsEntry": {
                      "defaultDisplayText": "Check if brand tag is available",
                      "checkType": "BOOLEAN"
                  },
                  "checkActionsEntry": {
                      "actionEntries": [
                          {
                              "expression": {
                                  "value": "yes",
                                  "operator": "EQUALS"
                              },
                              "actionCodeMetaData": {
                                  "actionCode": "CORRECT_PRODUCT",
                                  "properties": {
                                      "qcStatus": "QC_PASS"
                                  }
                              }
                          },
                          {
                              "expression": {
                                  "value": "yes",
                                  "operator": "NOT_EQUALS"
                              },
                              "actionCodeMetaData": {
                                  "actionCode": "BRAND_TAG_MISSING",
                                  "properties": {
                                      "qcStatus": "QC_FAIL"
                                  }
                              }
                          }
                      ]
                  }
              },
              {
                  "checkId": 119,
                  "ruleIds": [
                      "c36b0dc0-646c-11ea-be51-a18b1ffbcaa3",
                      "90d6e730-646c-11ea-be51-a18b1ffbcaa3"
                  ],
                  "checkDetailsEntry": {
                      "defaultDisplayText": "Enter the size on the box",
                      "checkType": "TEXT"
                  },
                  "checkActionsEntry": {
                      "actionEntries": [
                          {
                              "expression": {
                                  "value": "L",
                                  "operator": "EQUALS"
                              },
                              "actionCodeMetaData": {
                                  "actionCode": "CORRECT_PRODUCT",
                                  "properties": {
                                      "qcStatus": "QC_PASS"
                                  }
                              }
                          },
                          {
                              "expression": {
                                  "value": "L",
                                  "operator": "NOT_EQUALS"
                              },
                              "actionCodeMetaData": {
                                  "actionCode": "SIZE_MISMATCH",
                                  "properties": {
                                      "qcStatus": "QC_FAIL"
                                  }
                              }
                          }
                      ]
                  }
              },
              {
                  "checkId": 121,
                  "ruleIds": [
                      "c36b0dc0-646c-11ea-be51-a18b1ffbcaa3",
                      "90d6e730-646c-11ea-be51-a18b1ffbcaa3"
                  ],
                  "checkDetailsEntry": {
                      "defaultDisplayText": "Enter the price on the box",
                      "checkType": "MRP"
                  },
                  "checkActionsEntry": {
                      "actionEntries": [
                          {
                              "expression": {
                                  "value": "9999",
                                  "operator": "EQUALS"
                              },
                              "actionCodeMetaData": {
                                  "actionCode": "CORRECT_PRODUCT",
                                  "properties": {
                                      "qcStatus": "QC_PASS"
                                  }
                              }
                          },
                          {
                              "expression": {
                                  "value": "9999",
                                  "operator": "NOT_EQUALS"
                              },
                              "actionCodeMetaData": {
                                  "actionCode": "MRP_MISMATCH",
                                  "properties": {
                                      "qcStatus": "QC_FAIL"
                                  }
                              }
                          }
                      ]
                  }
              },
              {
                  "checkId": 122,
                  "ruleIds": [
                      "c36b0dc0-646c-11ea-be51-a18b1ffbcaa3",
                      "90d6e730-646c-11ea-be51-a18b1ffbcaa3"
                  ],
                  "values": [
                      "yellow",
                      "red",
                      "black",
                      "None of the above"
                  ],
                  "checkDetailsEntry": {
                      "defaultDisplayText": "Enter colour on the box",
                      "checkType": "RANDOM"
                  },
                  "checkActionsEntry": {
                      "actionEntries": [
                          {
                              "expression": {
                                  "value": "black",
                                  "operator": "EQUALS"
                              },
                              "actionCodeMetaData": {
                                  "actionCode": "CORRECT_PRODUCT",
                                  "properties": {
                                      "qcStatus": "QC_PASS"
                                  }
                              }
                          },
                          {
                              "expression": {
                                  "value": "black",
                                  "operator": "NOT_EQUALS"
                              },
                              "actionCodeMetaData": {
                                  "actionCode": "COLOUR_MISMATCH",
                                  "properties": {
                                      "qcStatus": "QC_FAIL"
                                  }
                              }
                          }
                      ]
                  }
              },
              {
                  "checkId": 123,
                  "ruleIds": [
                      "c36b0dc0-646c-11ea-be51-a18b1ffbcaa3",
                      "90d6e730-646c-11ea-be51-a18b1ffbcaa3"
                  ],
                  "values": [
                      "yes",
                      "no"
                  ],
                  "checkDetailsEntry": {
                      "defaultDisplayText": "Check if image of the product same as screen image",
                      "checkType": "BOOLEAN"
                  },
                  "checkActionsEntry": {
                      "actionEntries": [
                          {
                              "expression": {
                                  "value": "yes",
                                  "operator": "EQUALS"
                              },
                              "actionCodeMetaData": {
                                  "actionCode": "CORRECT_PRODUCT",
                                  "properties": {
                                      "qcStatus": "QC_PASS"
                                  }
                              }
                          },
                          {
                              "expression": {
                                  "value": "yes",
                                  "operator": "NOT_EQUALS"
                              },
                              "actionCodeMetaData": {
                                  "actionCode": "IMAGE_MISMATCH",
                                  "properties": {
                                      "qcStatus": "QC_FAIL"
                                  }
                              }
                          }
                      ]
                  }
              },
              {
                  "checkId": 126,
                  "ruleIds": [
                      "90d6e730-646c-11ea-be51-a18b1ffbcaa3"
                  ],
                  "checkDetailsEntry": {
                      "defaultDisplayText": "Enter the size of left piece of shoes/slipers/sandals etc",
                      "checkType": "TEXT"
                  },
                  "checkActionsEntry": {
                      "actionEntries": [
                          {
                              "expression": {
                                  "value": "L",
                                  "operator": "EQUALS"
                              },
                              "actionCodeMetaData": {
                                  "actionCode": "CORRECT_PRODUCT",
                                  "properties": {
                                      "qcStatus": "QC_PASS"
                                  }
                              }
                          },
                          {
                              "expression": {
                                  "value": "L",
                                  "operator": "NOT_EQUALS"
                              },
                              "actionCodeMetaData": {
                                  "actionCode": "SIZE_MISMATCH",
                                  "properties": {
                                      "qcStatus": "QC_FAIL"
                                  }
                              }
                          }
                      ]
                  }
              },
              {
                  "checkId": 127,
                  "ruleIds": [
                      "90d6e730-646c-11ea-be51-a18b1ffbcaa3"
                  ],
                  "checkDetailsEntry": {
                      "defaultDisplayText": "Enter the size of right piece of shoes/slipers/sandals etc",
                      "checkType": "TEXT"
                  },
                  "checkActionsEntry": {
                      "actionEntries": [
                          {
                              "expression": {
                                  "value": "L",
                                  "operator": "EQUALS"
                              },
                              "actionCodeMetaData": {
                                  "actionCode": "CORRECT_PRODUCT",
                                  "properties": {
                                      "qcStatus": "QC_PASS"
                                  }
                              }
                          },
                          {
                              "expression": {
                                  "value": "L",
                                  "operator": "NOT_EQUALS"
                              },
                              "actionCodeMetaData": {
                                  "actionCode": "SIZE_MISMATCH",
                                  "properties": {
                                      "qcStatus": "QC_FAIL"
                                  }
                              }
                          }
                      ]
                  }
              },
              {
                  "checkId": 124,
                  "ruleIds": [
                      "c36b0dc0-646c-11ea-be51-a18b1ffbcaa3",
                      "90d6e730-646c-11ea-be51-a18b1ffbcaa3"
                  ],
                  "values": [
                      "yes",
                      "no"
                  ],
                  "checkDetailsEntry": {
                      "defaultDisplayText": "Does the product look used/have strain",
                      "checkType": "BOOLEAN"
                  },
                  "checkActionsEntry": {
                      "actionEntries": [
                          {
                              "expression": {
                                  "value": "yes",
                                  "operator": "NOT_EQUALS"
                              },
                              "actionCodeMetaData": {
                                  "actionCode": "CORRECT_PRODUCT",
                                  "properties": {
                                      "qcStatus": "QC_PASS"
                                  }
                              }
                          },
                          {
                              "expression": {
                                  "value": "yes",
                                  "operator": "EQUALS"
                              },
                              "actionCodeMetaData": {
                                  "actionCode": "STRAIN/DIRTY",
                                  "properties": {
                                      "qcStatus": "QC_FAIL"
                                  }
                              }
                          }
                      ]
                  }
              },
              {
                  "checkId": 125,
                  "ruleIds": [
                      "c36b0dc0-646c-11ea-be51-a18b1ffbcaa3",
                      "90d6e730-646c-11ea-be51-a18b1ffbcaa3"
                  ],
                  "values": [
                      "yes",
                      "no"
                  ],
                  "checkDetailsEntry": {
                      "defaultDisplayText": "Is the product damage from any place",
                      "checkType": "BOOLEAN"
                  },
                  "checkActionsEntry": {
                      "actionEntries": [
                          {
                              "expression": {
                                  "value": "yes",
                                  "operator": "NOT_EQUALS"
                              },
                              "actionCodeMetaData": {
                                  "actionCode": "CORRECT_PRODUCT",
                                  "properties": {
                                      "qcStatus": "QC_PASS"
                                  }
                              }
                          },
                          {
                              "expression": {
                                  "value": "yes",
                                  "operator": "EQUALS"
                              },
                              "actionCodeMetaData": {
                                  "actionCode": "DAMAGED_PRODUCT",
                                  "properties": {
                                      "qcStatus": "QC_FAIL"
                                  }
                              }
                          }
                      ]
                  }
              }
          ]
      }
  ]
}