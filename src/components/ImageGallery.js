import './image-gallery.css';
import React, { PureComponent } from 'react';

// props: list: *array of item, onClickImage: ?function  
// item = { *image, *thumbnail, key: ?any, webp: ?.webp format}
// @TODO : Enable zoom (roll over zoom)
class ImageGallery extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { selected: 0 };
    this.onClickImage = this.onClickImage.bind(this);
  }
  onClickThumbnail(index) {
    this.setState({ selected: index })
  }
  onClickImage() {
    const { list, onClickImage } = this.props;
    if (typeof onClickImage === 'function') {
      const { selected } = this.state;
      onClickImage({ selected: list[selected], index: selected })
    }
  }
  render() {
    const { list = [], height, imageToShow } = this.props;
    const { selected } = this.state;
    const selectedImage = list[selected] ? list[selected][imageToShow] : undefined;
    return (
      <div className="x-image-gallary">
        <div className="thumbnails" style={{ height }}>
          {list.map((item, index) => (
            <div key={item.key || index} className="thumbnail" onClick={() => this.onClickThumbnail(index)}>
              <picture>
                {/* {item.webp && <source srcset={item.webp} type="image/webp" />} */}
                <img src={item.thumbnail}></img>
              </picture>
            </div>
          ))}
        </div>
        <div className="image" style={{ height }} onClick={this.onClickImage}>
          <picture>
            {/* {list[selected].webp && <source srcset={list[selected].webp} type="image/webp" />} */}
            <img src={selectedImage} style={{ height: '100%' }}></img>
          </picture>
        </div>
        {/* @TODO: This component should be responsible for zoom */}
      </div>
    );
  }
}

export default ImageGallery;
