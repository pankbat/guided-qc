export const get = (object, keyStr = '', defaultValue) => {
  if (!keyStr || typeof object !== 'object') return defaultValue;

  const keys = keyStr.split('.');
  const res = keys.reduce((data, key) => {
    return data ? data[key] : data;
  }, object);
  
  return res || defaultValue;
};