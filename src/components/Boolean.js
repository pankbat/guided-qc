import React, { PureComponent } from 'react';

class BooleanComponent extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { active: '' };
    this.onKeydown = this.onKeydown.bind(this);
  }
  componentDidMount() {
    document.addEventListener('keydown', this.onKeydown);
  }
  componentWillUnmount() {
    document.removeEventListener('keydown', this.onKeydown);
  }
  onKeydown(event) {
    if (event.which === 89) { // Y/y (89) to Yes
      this.setValue('YES');
    } else if (event.which === 78) { // n/N (78) to No
      this.setValue('NO');
    }
    event.stopPropagation();
  }
  onClick(value) {
    this.setValue(value);
  }
  setValue(value) {
    this.setState({ active: value }, () => {
      setTimeout(() => {
        this.setState({ active: '' }, () => this.props.setValue(value));
      }, 500);
    });
  }
  render() {
    const { active } = this.state;
    return (
      <div>
        <button className={`x-btn x-btn-default display-block mr-auto ${active === 'NO' ? 'x-active' : ''}`} onClick={() => this.onClick('NO')}>
          <div className="fw500 fsp20">No</div>
          <div className="fw300 fsp14 mr-t5 italic">Press&nbsp;<b>N</b>&nbsp;for No</div>
        </button>
        <button className={`x-btn x-btn-default display-block mr-auto ${active === 'YES' ? 'x-active' : ''}`} style={{ marginTop: '85px' }} onClick={() => this.onClick('YES')}>
          <div className="fw500 fsp20">Yes</div>
          <div className="fw300 fsp14 mr-t5 italic">Press&nbsp;<b>Y</b>&nbsp;for Yes</div>
        </button>
      </div>
    );
  }
}

export default BooleanComponent;
