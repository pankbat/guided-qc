import { get } from "./get";

const neg_operators = {
  'BETWEEN': 'NOT_IN',
  'EQUALS': 'NOT_EQUALS',
  'INCLUDES': 'NOT_INCLUDES'
};

function RuleEngine() {
  this.state = {};
}

function parseEntries(entries) {
  return entries.reduce((obj, entry) => {
    let value, min, max;        
    const operator = get(entry, 'expression.operator');
    const actionCode = get(entry, 'actionCodeMetaData.actionCode');
    const qcStatus = get(entry, 'actionCodeMetaData.properties.qcStatus');
    
    if (operator == 'BETWEEN') {
      min = get(entry, 'expression.minValue');
      max = get(entry, 'expression.maxValue');
    } else {
      value = get(entry, 'expression.value');
    }

    obj[operator] = { value, actionCode, qcStatus, min, max };
    
    return obj;
  }, {});
}

const equals = (value, input) => {
  if (typeof value === 'string' && typeof input === 'string') {
    return input.toUpperCase() === value.toUpperCase(); 
  }
  return input == value;
};

function evaluate(operator, input, parsedEntries) {
  let value;
  switch(operator) {
    case 'BETWEEN':
      const min = get(parsedEntries, 'BETWEEN.min');
      const max = get(parsedEntries, 'BETWEEN.max');
      return (parseFloat(input) >= min && parseFloat(input) <= max);
    case 'EQUALS':
      value = get(parsedEntries, 'EQUALS.value.0');
      return equals(value, input);
    case 'INCLUDES':
      value = get(parsedEntries, 'INCLUDES.value');
      if (value && typeof (value.some) === 'function') {
        return value.some(val => {
          return equals(val, input);
        })
      } 
  }
}

function getEntity(parsedEntries, operator) {
  const actionCode = get(parsedEntries, `${operator}.actionCode`);
  const qcStatus = get(parsedEntries, `${operator}.qcStatus`);
  if (qcStatus === 'QC_FAIL') this.state = { qcStatus: 'QC_FAIL' };
  return { actionCode, qcStatus };
}

RuleEngine.prototype.apply = function(input, entries) {
  const parsedEntries = parseEntries(entries);
  
  let entity;
  ['BETWEEN', 'EQUALS', 'INCLUDES'].map(operator => {
    if (parsedEntries[operator]) {
      if (evaluate(operator, input, parsedEntries)) {
        entity = getEntity.call(this, parsedEntries, operator);
      } else {
        entity = getEntity.call(this, parsedEntries, neg_operators[operator]);
      }
    }
  });

  return entity;
}

RuleEngine.prototype.getState = function() {
  return this.state;
}

export default RuleEngine;