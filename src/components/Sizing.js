import React, { PureComponent } from 'react';

class Sizing extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { value: '' };
    this.onEnter = this.onEnter.bind(this);
    this.onChange = this.onChange.bind(this);
    this.setValue = this.setValue.bind(this);
  }
  componentDidMount() {
    document.addEventListener('keydown', this.onEnter);
  }
  componentWillUnmount() {
    document.removeEventListener('keydown', this.onEnter);
  }
  onChange(event) {
    this.setState({ value: event.target.value });
  }
  onEnter(event) {
    if (event.which === 13) {
      this.setValue(this.state.value);
    }
    event.stopPropagation();
  }
  setValue() {
    let { value } = this.state;
    value = parseFloat(value);
    if (isNaN(value)) value = undefined;
    else value = value.toFixed(1);
    this.setState({ value: '' }, () => this.props.setValue(value));
  }
  render() {
    const { value } = this.state;
    const { guideline } = this.props;
    return (
      <div>
        <input className="x-input mr-b30" type="number" value={value} onChange={this.onChange} autoFocus />
        <div className="guideline mr-b20">{guideline && <img src={guideline} />}</div>
        <button className="x-btn x-btn-primary display-block mr-auto" onClick={this.setValue} disabled={!value}>
          <div className="fw500 fsp20">Done</div>
          <div className="fw300 fsp14 mr-t5 italic">Press&nbsp;<b>Enter</b>&nbsp;to confirm</div>
        </button>
      </div>
    );
  }
}
export default Sizing;
