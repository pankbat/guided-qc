import './random.css';
import React from 'react';

class Random extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }
  setValue(value) {
    const { setValue } = this.props;
    this.setState({ value }, () => {
      setTimeout(() => setValue(value), 500);
    });
  }
  render() {
    const { values: options = [] } = this.props;
    const { value } = this.state;
    return (
      <div className="x-random">
        {options.map(option => (
          <div key={option} onClick={() => this.setValue(option)} className={`x-random-option ${value === option ? 'active' : ''}`}>
            {value === option ? <span className="icon-circle">&#10004;</span> : null}
            <span className="flex1">{option}</span>
          </div>
        ))}
      </div>
    );
  }
}

export default Random;
