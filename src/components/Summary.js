import './summary.css';
import React from 'react';
import { get } from '../utils/get';

const Summary = (props) => {
  return (<div className="guided-summary text-left">
    {props.list.map((item, index) => {
      let question = get(item, 'enrichedCheckEntry.checkDetailsEntry.defaultDisplayText', '');
      return (
        <div className="item" key={index}>
          <div className="text-capitalize" style={{ color: 'rgba(0, 0, 0, 0.5)', display: 'flex' }}>
            <span className="inline-block" style={{ width: '20px' }}>{index + 1}.</span>
            <span className="inline-block" style={{ maxWidth: '250px'}}>{question}</span>
          </div>
          <div className="text-uppercase">{item.input}</div>
        </div>
      )
    })}
  </div>);
};

export default Summary;
