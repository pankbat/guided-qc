import './barcode.css'
import React, { PureComponent } from 'react';
import debounce from '../utils/debounce';

const barcodeIcon = (
  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
    <path fillOpacity=".87" fillRule="evenodd" d="M7.333 2.833c0-.091-.075-.166-.166-.166h-1c-.092 0-.167.075-.167.166V6h1.333V2.833zm-2.666 0c0-.091-.075-.166-.167-.166H2.833c-.091 0-.166.075-.166.166V6h2V2.833zm-2 10.334c0 .091.075.166.166.166H4.5c.092 0 .167-.075.167-.166V10h-2v3.167zm8.666-10.334c0-.091-.075-.166-.166-.166h-1c-.092 0-.167.075-.167.166V6h1.333V2.833zm2 0c0-.091-.075-.166-.166-.166h-1c-.092 0-.167.075-.167.166V6h1.333V2.833zm-4 0c0-.091-.075-.166-.166-.166h-.334c-.091 0-.166.075-.166.166V6h.666V2.833zM12 13.167c0 .091.075.166.167.166h1c.091 0 .166-.075.166-.166V10H12v3.167zm-6 0c0 .091.075.166.167.166h1c.091 0 .166-.075.166-.166V10H6v3.167zm4 0c0 .091.075.166.167.166h1c.091 0 .166-.075.166-.166V10H10v3.167zm-1.333 0c0 .091.075.166.166.166h.334c.091 0 .166-.075.166-.166V10h-.666v3.167zM14.5 7.333h-13c-.092 0-.167.075-.167.167v1c0 .092.075.167.167.167h13c.092 0 .167-.075.167-.167v-1c0-.092-.075-.167-.167-.167z" />
  </svg>
);

class Barcode extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { errorMessage: '', value: '' };
    this.setRef = this.setRef.bind(this);
    this.onInput = this.onInput.bind(this);
    this.onKeydown = this.onKeydown.bind(this);
    this.clear = debounce(this.clear.bind(this), props.scannerTimeout || 50);
  }
  componentDidMount() {
    document.addEventListener('keydown', this.onKeydown);
    this.input.addEventListener('paste', this.onPaste);
  }
  componentWillUnmount() {
    document.removeEventListener('keydown', this.onKeydown);
    this.input.removeEventListener('paste', this.onPaste);
  }
  setRef(element) {
    this.input = element;
  }
  onInput(event) {
    this.setState({ value: event.target.value });
    this.clear();
  }
  onPaste(e) {
    e.preventDefault()
  }
  onKeydown(event) {
    const { value } = this.state;
    if (event.which === 13 && value) {
      this.setValue(value);
    }
    event.stopPropagation();
  }
  setValue(value) {
    if (typeof value === 'string') {
      // Remove leading zeros & trim the input
      value = value.trim().replace(/^0+/, '');
    }
    if (value == this.props.itemBarcode) {
      this.setState({ errorMessage: 'Please scan valid barcode', value: '' });
      return;
    } 
    this.props.setValue(value);
    this.setState({ errorMessage: '', value: '' });
  }
  clear() {
    this.setState({ value: '' });
  }
  render() {
    const { guideline } = this.props;
    const { value, errorMessage } = this.state;
    return (
      <div className="barcodeq">
        <div style={{ position: 'relative', display: 'inline-block' }}>
          <div style={{ position: 'absolute', top: '13px', left: '8px' }}>{barcodeIcon}</div>
          <input value={this.state.value} onChange={this.onInput} ref={this.setRef} className="x-input" autoFocus />
        </div>
        {errorMessage && <div className="x-error mr-b30">{errorMessage}</div>}
        <div className="guideline mr-b20">{guideline && <img src={guideline} />}</div>
        <button className="x-btn x-btn-primary display-block mr-auto" onClick={() => this.setValue(value)} disabled={!value}>
          <div className="fw500 fsp20">Done</div>
          <div className="fw300 fsp14 mr-t5 italic">Press&nbsp;<b>Enter</b>&nbsp;to confirm</div>
        </button>
      </div>
    );
  }
}

export default Barcode;
